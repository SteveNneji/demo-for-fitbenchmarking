import math
import numpy as np
from scipy import optimize
from robotics import goniometer, two_link, table, enginx_combo

def matrix_to_emap(matrix):
    """Converts matrix to exponential map

    :param matrix: matrix
    :type matrix: Matrix44
    """
    r = matrix[0:3, 0:3]
    b = r - np.identity(3)

    _, _, v = np.linalg.svd(b)

    axis = v[-1, :]

    twocostheta = np.trace(r) - 1
    twosinthetav = [r[2, 1]-r[1, 2], r[0, 2]-r[2, 0], r[1, 0]-r[0, 1]]
    twosintheta = np.dot(axis, twosinthetav)

    angle = math.degrees(math.atan2(twosintheta, twocostheta))

    return angle * axis 

def cost_function(q, ri, target_pose):
    """Cost function for inverse kinematics

    :param q: list of joint offsets to move to. The length must be equal to number of links
    :type q: List[float]
    :param ri: robot
    :type ri: SerialManipulator
    :param target_pose: desired pose
    :type target_pose: Matrix44
    """
    current_pose = ri.fkine(q)
    error = np.zeros(6)
    error[:3] = target_pose[:3, 3] - current_pose[:3, 3]
    
    diff = target_pose[:3, :3] @ current_pose[:3, :3].transpose()
    error[3:6] = matrix_to_emap(diff)

    err = np.dot(error, error)
    return err


def numeric_inverse_kinematics(ri, q0, target_pose, tol):
    """Numerical inverse kinematics solver

    :param q: list of joint offsets to move to. The length must be equal to number of links
    :type q: List[float]
    :param ri: robot
    :type ri: SerialManipulator
    :param target_pose: desired pose
    :type target_pose: Matrix44
    :param tol: tolerance for optimizer
    :type tol: float
    """
    def callback(_x, f, _context):
        if f < tol:
            return True

        return False
    
    bounds = [(link.lower_limit, link.upper_limit) for link in ri.links]
    r = optimize.dual_annealing(cost_function, bounds, args=(ri, target_pose), x0=q0, local_search_options={'method': 'L-BFGS-B', 'options':{'maxiter':1000}},
                                callback=callback, seed=10, initial_temp=500, no_local_search=False)

    # r = optimize.minimize(cost_function, q0, args=(ri, target_pose), bounds=bounds, method='SLSQP', options={'maxiter':5000})                            
    print(r.message, '\nNumber of Iterations: ', r.nit, r.x)

    return r.x


if __name__ == '__main__':
    ri = two_link()
    target = ri.fkine([np.pi/2, -np.pi/2])
    print(numeric_inverse_kinematics(ri, [0., 0.], target, 0.001))
   
    ri = table()
    target = ri.fkine([109.23, np.pi/2, -100, 40])
    print(numeric_inverse_kinematics(ri, [0., 0., 0., 0.], target, 0.001))
    
    ri = goniometer()
    target = ri.fkine([np.pi/2, -np.pi/3, -np.pi/4])
    print(numeric_inverse_kinematics(ri, [-np.pi/2, np.pi/2, np.pi/4], target, 0.001))
    
    ri = enginx_combo()
    target = ri.fkine([24, np.pi/5, -103, -125, -np.pi/2, -np.pi/2, np.pi/1.1])
    print(numeric_inverse_kinematics(ri, np.zeros(7), target, 0.001))

 